import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http, Headers, RequestOptions } from '@angular/http';

import { ToastController } from 'ionic-angular';

import 'rxjs/add/operator/map';

import { Camera, CameraOptions } from '@ionic-native/camera';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  beer = {nome: "", tagline: "", descricao: "", img: ""};

  private url: String = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public toastCtrl: ToastController, public camera: Camera) {
    

  }

  saveBeer(beer) {
    console.log(beer);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let requestOptions = new RequestOptions({ headers: headers });


    this.http.post(this.url + "add_beer", beer, requestOptions)
            .map(res => { res.json(); })
            .subscribe( 
              data => {
                console.log(data);
                // this.presentToast(data.message);
              }
            )
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: '' + message,
      duration: 3000
    });
    toast.present();
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.beer.img = base64Image;
      console.log(base64Image);

    }, (err) => {

    });

  }

  

}
