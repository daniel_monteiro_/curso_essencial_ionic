import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AlertController } from 'ionic-angular';

import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {

  private url: String = 'https://api.punkapi.com/v2/';

  public beer: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public http: Http) {

    let id = this.navParams.get("beer_id");

    this.http.get(this.url + '/beers/' + id)
    .map(res => res.json())
    .subscribe(data => {
      this.beer = data[0];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPage');
  }
}