import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(public http: Http, public storage: Storage) {
    console.log('Hello AuthProvider Provider');
  }

  login() {

  }

  userIsLogged() {
    return this.storage.get('token').then(val => {
      if (val !== undefined) {
        return val;
      }else {
        return false;
      }
    });
  }

  logout() {
    this.storage.remove('token');
  }

}
